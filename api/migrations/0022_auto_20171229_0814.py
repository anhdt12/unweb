# Generated by Django 2.0 on 2017-12-29 08:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0021_action_location_name'),
    ]

    operations = [
        migrations.RenameField(
            model_name='action',
            old_name='folow_users',
            new_name='follow_users',
        ),
    ]
