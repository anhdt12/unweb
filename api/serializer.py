from django.contrib.auth.models import User
from rest_framework import serializers
from django.contrib.auth import authenticate, login, logout

from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from django.http import JsonResponse
import random
import json


from api.models import Slider,ImageSlider,Post,Target,MainTarget,Article,ArticleMeta,AppUser,Idea,Action,Collection



class AppUserInfoSerializer(serializers.ModelSerializer):
    position = serializers.CharField(source="appuser.position")
    is_invidual = serializers.BooleanField(source="appuser.is_invidual")
    auth_token = serializers.CharField(required=False)
    class Meta:
        model = User
        fields = ("id","auth_token",'username','email','position','is_invidual',"web","intro",)
class MainTargetListSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = MainTarget
class AppUserSerializer(serializers.ModelSerializer):
    position = serializers.CharField(source="appuser.position")
    is_invidual = serializers.BooleanField(source="appuser.is_invidual")
    auth_token = serializers.CharField(required=False)
    birthday = serializers.DateField(source="appuser.birthday",allow_null=True,required=False)
    image_url = serializers.CharField(source="appuser.image_url")
    location = serializers.CharField(source="appuser.location")
    phone = serializers.CharField(source="appuser.phone",allow_null=True,required=False)
    website = serializers.CharField(source="appuser.website",allow_blank=True,allow_null=True,required=False)
    intro = serializers.CharField(source="appuser.intro",allow_blank=True,allow_null=True,required=False)
    org_type = serializers.CharField(source="appuser.org_type",allow_blank=True,allow_null=True,required=False)
    interested_SDGs = MainTargetListSerializer(source="appuser.interested_SDGs",many=True)
    class Meta:
        model = User
        fields = ("id","auth_token","location","org_type","website","phone","intro",'last_name','username','interested_SDGs','password','email','position','is_invidual',"birthday","image_url")

    def create(self, validated_data):
        """
        Given a dictionary of deserialized field values, either update
        an existing model instance, or create a new model instance.
        """
        print(validated_data)
        user = User.objects.create_user(username=validated_data.get('username'),password=validated_data.get('password'),email=validated_data.get('email'),last_name=validated_data.get('last_name'))
        print(user,user.password,authenticate(username=user.username,password=user.password))
        invidual = self.initial_data['is_invidual']
        print("dmkvfsmrfasdfa",invidual)
        if invidual == False:
            AppUser.objects.create(location=self.initial_data.pop("location"),org_type=self.initial_data.pop("org_type"),user=user,image_url=self.initial_data.pop("image_url"),position=self.initial_data.pop('position'),is_invidual=self.initial_data.pop('is_invidual'))
        else :
            AppUser.objects.create(location=self.initial_data.pop("location"),user=user,image_url=self.initial_data.pop("image_url"),position=self.initial_data.pop('position'),is_invidual=self.initial_data.pop('is_invidual'),birthday=self.initial_data.pop('birthday'))
        user.password = ""
        return user
class MainTargetSerializer(serializers.ModelSerializer):

    def get_childTargets(self, obj):
        childTargets = Target.objects.filter(main_target__title=obj.title)
        return TargetSerializer(childTargets, many=True).data

    childTarget = serializers.SerializerMethodField('get_childTargets')

    class Meta:
        fields = "__all__"
        extra_fields = ['childTarget',]
        model = MainTarget
class MetaArticleSerialize(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model  = ArticleMeta
class TargetSerializer(serializers.ModelSerializer):
    def get_childTargets(self, obj):
        childTargets = Target.objects.filter(parentTarget__title=obj.title)
        return TargetSerializer(childTargets, many=True).data

    childTarget = serializers.SerializerMethodField('get_childTargets')
    class Meta:
        fields = ('id','title','description','childTarget')
        model = Target
class PostSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = Post

class LabelGameConnectSerializer(serializers.Serializer):
    label = serializers.CharField(max_length=50000)
    id = serializers.IntegerField()
class ImageGameConnectSerializer(serializers.Serializer):
    img = serializers.CharField(max_length=50000)
    id = serializers.IntegerField()
class DataGameConnectSerializer(serializers.Serializer):
    quotes = LabelGameConnectSerializer(many=True)
    images = ImageGameConnectSerializer(many=True)
    success_images = ImageGameConnectSerializer(many=True)
class ArticleSerializer(serializers.ModelSerializer):
    def get_meta_datas(self,obj):
        print("e3331",obj)
        meta = ArticleMeta.objects.filter(article__article_name=obj.article_name)
        print("e3332221",meta)
        return MetaArticleSerialize(meta,many=True).data
    meta_data = serializers.SerializerMethodField('get_meta_datas')
    def get_user(self,obj):
        print("objasda",obj.user)
        if obj.user is not None:
            user = AppUser.objects.filter(user__id=obj.user.id).first()
            return UserInfoSerialzer(user).data
        else:
            return None
    user_detail = serializers.SerializerMethodField("get_user")
    class Meta:
        fields = '__all__'
        extra_fields = ['meta_data',"user_detail"]
        model = Article

class ActionsByOrg(serializers.Serializer):
    def org(self,obj):
        return UserInfoSerialzer(obj).data
    organization = serializers.SerializerMethodField("org")
    def actionsOrg(self,obj):
        actions = Action.objects.filter(user_id=obj.user.id)
        data = ActionSerializer(actions,many=True).data
        for action in data:
            user = action.pop("user")
        return data
    actions = serializers.SerializerMethodField("actionsOrg")
class ActionsBySDG(serializers.Serializer):
    def SDGObject(self,obj):
        return MainTargetListSerializer(obj).data
    SDGs = serializers.SerializerMethodField("SDGObject")
    def actionsSGD(self,obj):
        actions = Action.objects.filter(SDG_id=obj.id)
        data = ActionSerializer(actions,many=True).data
        return data
    actions = serializers.SerializerMethodField("actionsSGD")


class UserInfoSerialzer(serializers.ModelSerializer):
    def lastName(self,obj):
        user = obj.user
        return user.last_name
    def get_mail(self,obj):
        user = obj.user
        return user.email
    last_name = serializers.SerializerMethodField("lastName")
    email = serializers.SerializerMethodField("get_mail")
    def getUserName(self,obj):
        user = obj.user
        return user.username
    user_name = serializers.SerializerMethodField("getUserName")
    class Meta:
        fields = "__all__"
        extra_fields = ["last_name","email","user_name"]
        model = AppUser
class ActionSerializer(serializers.ModelSerializer):
    def get_action(self, obj):
        actions = obj.actions
        return json.loads(actions)
    def get_image(self, obj):
        images = obj.images
        return json.loads(images)
    def number_of_follow(self,obj):
        followers = obj.follow_users.all()
        print("sadsadas",len(followers))
        return len(followers)
    follows_count = serializers.SerializerMethodField("number_of_follow")
    follow_users = UserInfoSerialzer(many=True)
    user = UserInfoSerialzer()
    SDG = MainTargetListSerializer(many=True)
    list_actions = serializers.SerializerMethodField("get_action")
    list_images = serializers.SerializerMethodField("get_image")
    class Meta:
        model = Action
        exclude = ["actions","images","SDG","search_name"]
        extra_fields = ['list_actions',"list_image","SDG_detail","follows_count"]
class IdeaSerializer(serializers.ModelSerializer):
    def get_image(self, obj):
        if obj.images is None:
            return ""
        data = ""
        try:
            images = obj.images
            data = json.loads(images)
        except:
            pass
        return data
    list_images = serializers.SerializerMethodField("get_image")
    likers = UserInfoSerialzer(many=True)
    class Meta:
        model = Idea
        exclude = ["images"]
        extra_fields = ["list_images"]
class ImageSerializer(serializers.ModelSerializer):
    def get_actionId(self,obj):
        action = obj.action
        if action is not None:
            actionId = obj.action.id
            return actionId
        return None

    def get_user(self, obj):
        action = obj.action
        if action is not None:
            user= obj.action.user
            return UserInfoSerialzer(user).data
        return None

    user = serializers.SerializerMethodField()

    def get_blogId(self, obj):
        blog = obj.blog
        if blog is not None:
            blogId = obj.blog.id
            collectionId = blog.collection.id
            blog = {}
            blog["collectionId"] = collectionId
            blog["blogId"] = blogId
            return blog
        return None

    action = serializers.SerializerMethodField("get_actionId")
    blog = serializers.SerializerMethodField("get_blogId")

    class Meta:
        fields = ('img_url','position','label','action','blog','user')
        model = ImageSlider
class AllSliderSerializer(serializers.ModelSerializer):
    def get_imageSlider(self, obj):
        images = ImageSlider.objects.filter(slider__id=obj.id).order_by("position")
        return ImageSerializer(images, many=True).data

    images = serializers.SerializerMethodField('get_imageSlider')

    class Meta:
        fields = ('page', 'position', 'images', 'slider_name')
        model = Slider