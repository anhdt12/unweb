from django.shortcuts import render
from django.http.response import HttpResponse
import urllib.parse, urllib.request, json
from rest_framework import permissions,status,generics
from django.contrib.auth.models import User
from django.db.models import Q
from rest_framework.decorators import api_view, authentication_classes, \
    permission_classes
import twitter
from requests_oauthlib import OAuth1

from functools import reduce
import random
import requests
import uuid
import json
import base64
import time
from hashlib import sha1
import hmac
import sys
from django.contrib.auth import authenticate, login, logout
from rest_framework.response import Response
from rest_framework.views import APIView
from api.models import Post,Slider,ImageSlider,Target,MainTarget,Article,ConnectGame,AppUser,Idea,Action,Collection,ArticleMeta
from api.serializer import AllSliderSerializer,PostSerializer,TargetSerializer,MainTargetSerializer,\
ArticleSerializer,DataGameConnectSerializer,AppUserSerializer,IdeaSerializer,MainTargetListSerializer,\
    ActionSerializer,UserInfoSerialzer
REQUEST_URL = "https://api.twitter.com/oauth/request_token"
TOKEN = "v8Tu8LJBIQVlMKmgPODS9BWow"
TOKEN_SECRET = "vePo7NhqBRUBy9S95Gqung9KmHVyWf95PjgyHIatwhDZUnmwrA"
s1 = u'ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹ'
s0 = u'AAAAEEEIIOOOOUUYaaaaeeeiioooouuyAaDdIiUuOoUuAaAaAaAaAaAaAaAaAaAaAaAaEeEeEeEeEeEeEeEeIiIiOoOoOoOoOoOoOoOoOoOoOoOoUuUuUuUuUuUuUuYyYyYyYy'
# Create your views here.
def remove_accents(input_str):
    s = ''
    print
    input_str.encode('utf-8')
    for c in input_str:
        if c in s1:
            s += s0[s1.index(c)]
        else:
            s += c
    return s
@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def twitterRequest(request):
    auth = OAuth1(TOKEN,TOKEN_SECRET)
    r = requests.post(url=REQUEST_URL,auth=auth)
    print("data ssssaas", r.content, ":\n", r.text)
    jsonString = '{ "' + r.text.replace('=', '": "').replace('&', '", "') + '"}'
    return Response(json.loads(jsonString))

@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def twitterLogin(request):
    oauth_verifier =request.GET.get("oauth_verifier","null")
    oauth_token =request.GET.get("oauth_token","null")
    form = {'oauth_verifier': oauth_verifier}
    urlVerify = "https://api.twitter.com/oauth/access_token?oauth_verifier"
    auth = OAuth1(TOKEN,TOKEN_SECRET,oauth_token)
    r = requests.post(url=urlVerify,auth=auth,data=form)
    jsonString = '{ "' + r.text.replace('=', '": "').replace('&', '", "') + '"}'
    oauthJson = json.loads(jsonString)
    api = twitter.Api(TOKEN,TOKEN_SECRET,oauthJson["oauth_token"],oauthJson["oauth_token_secret"])
    userRequest = api.VerifyCredentials(include_email=True)
    user = User.objects.filter(username=userRequest.screen_name).first()
    if not user:
        user = User.objects.create_user(email=userRequest.email, username=userRequest.screen_name, password=uuid.uuid4(),
                                        last_name=userRequest.name)
        image_url = userRequest.profile_image_url_https
        print(image_url)
        AppUser.objects.create(user=user, is_invidual=True, image_url=image_url)
    appUser = AppUser.objects.filter(user_id=user.id).first()
    if appUser:
        user.position = appUser.position
        user.is_invidual = appUser.is_invidual
        user.birthday = appUser.birthday
        user.image_url = appUser.image_url
    serializer = AppUserSerializer(user)
    data = serializer.data
    password = data.pop("password")
    return Response(data)
@authentication_classes([])
@permission_classes([])
class SliderList(APIView):
    def get(self, request):
        slider = Slider.objects.all()
        serializer = AllSliderSerializer(slider, many=True)
        return Response(serializer.data)
@authentication_classes([])
@permission_classes([])
class PostList(APIView):
    def get(self, request):
        page = request.GET.get('page','home')
        post = Post.objects.filter(page=page)
        serializer = PostSerializer(post, many=True)
        return Response(serializer.data)
@authentication_classes([])
@permission_classes([])
class ArticleList(APIView):
    def get(self, request):
        collection = request.GET.get('collection','')
        userId = request.GET.get("userId",'')
        articles = Article.objects.all()
        if collection != '':
            articles = articles.filter(collection__collection_name=collection)
        if userId != '':
            articles = articles.filter(user=userId)
        serializer = ArticleSerializer(articles, many=True)
        return Response(serializer.data)
@authentication_classes([])
@permission_classes([])
class TargetList(APIView):
    def get(self, request):
        targets = MainTarget.objects.all()
        serializer = MainTargetSerializer(targets, many=True)
        return Response(serializer.data)
class GameConnect(APIView):
    @authentication_classes([])
    @permission_classes([])
    def get(self,request):
        quizs = ConnectGame.objects.all()
        quotes = []
        images = []
        success_imgs = []
        for quiz in quizs:
            quote = {
                "label":quiz.label,
                "id":quiz.id
            }
            print (quote)
            quotes.append(quote)
            img = {
                "img":quiz.img_url,
                "id":quiz.id
            }
            success_img = {
                "img": quiz.success_img_url,
                "id": quiz.id
            }
            success_imgs.append(success_img)
            images.append(img)
        random.shuffle(quotes)
        random.shuffle(images)
        random.shuffle(success_imgs)
        response = {
            "quotes":quotes,
            "images":images,
            "success_images":success_imgs
        }
        response = DataGameConnectSerializer(response)
        print (response.data)
        return Response(response.data)
class ArticlePost(APIView):
    @authentication_classes([])
    @permission_classes([])
    def post(self,request):
        userId = request.data.pop("userId")
        user = User.objects.filter(id=userId).first()
        collectionId = request.data.pop("collectionId")
        collection = Collection.objects.filter(id=collectionId).first()
        meta_data = request.data.pop("meta_data")
        article = Article.objects.create(user=user,collection=collection,**request.data)
        # article_meta = ArticleMeta.objects.create()
        serializer = ArticleSerializer(article)
        print ("21asdasdas",serializer)
        return Response(data=serializer.data,status=status.HTTP_200_OK)

@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def edit_article(request):
    article_id = request.data.pop("articleId")
    meta_data = request.data.pop("meta_data")
    collectionId = request.data.pop("collectionId")
    collection = Collection.objects.filter(id=collectionId).first()
    article = Article.objects.filter(id=article_id).update(search_article_name=remove_accents(request.data["article_name"]),collection=collection,**request.data)
    # article_meta = ArticleMeta.objects.create()
    data = {
        "message": "Update successfully"
    }
    return Response(data=data, status=status.HTTP_200_OK)
@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def get_Blog(request):
    collection = ['Volunteer', 'fund', 'Stories']
    articles = Article.objects.filter(
        (reduce(lambda x, y: x | y, [Q(collection__collection_name=item) for item in collection])))
    serializer = ArticleSerializer(articles[:15], many=True)
    return Response(serializer.data)
@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def search_video(request):
    allArticales = Article.objects.all()
    for article in allArticales:
        article.search_article_name =  remove_accents(article.article_name)
        article.save()
    keyword = remove_accents(request.GET.get('keyword'))
    print("hisadasd",keyword)
    articles = Article.objects.filter(collection__collection_name="Videos").filter(search_article_name__contains=keyword)
    serializer = ArticleSerializer(articles, many=True)
    return Response(serializer.data)
@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def create_user(request):
    users = User.objects.filter(email=request.data["email"]).first()
    print("ádsadasda",users)
    if users is not None:
        data = {
            "message": "Email have been used for another account"
        }
        return Response(data=data,status=status.HTTP_400_BAD_REQUEST)
    request_data = request.data
    request_data["interested_SDGs"] = []
    serialized = AppUserSerializer(data=request_data)
    if serialized.is_valid():
        serialized.save()
        data = serialized.data
        password = data.pop("password")
        return Response(serialized.data, status=status.HTTP_201_CREATED)
    else:
        return Response(serialized._errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def loggingFacebook(request):
    access_token = request.data['accessToken']
    r = requests.get(url='https://graph.facebook.com/v2.11/me?fields=id%2Cname%2Cpicture&access_token='+access_token)
    data = r.json()
    user = User.objects.filter(username=data.get('id')).first()
    if not user:
        user = User.objects.create_user(email=request.data["email"],username=data.get('id'),password=uuid.uuid4(),last_name=data.get('name'))
        image_url = data.get("picture").get("data").get("url")
        AppUser.objects.create(user=user,is_invidual=request.data["is_invidual"],image_url=image_url)
    appUser = AppUser.objects.filter(user_id=user.id).first()
    if appUser:
        user.position = appUser.position
        user.is_invidual = appUser.is_invidual
        user.birthday = appUser.birthday
        user.image_url = appUser.image_url
    serializer = AppUserSerializer(user)
    password = serializer.data.pop("password")
    return Response(serializer.data)
@api_view(['GET'])
@authentication_classes([])
@permission_classes([])
def getUserbyName(request):
    userName = request.GET.get('userName')
    user = AppUser.objects.filter(user__username=userName).first()
    serializer = UserInfoSerialzer(user)
    data = serializer.data
    return Response(data=data, status=status.HTTP_200_OK)
@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def loggingGoogle(request):
    access_token = request.data['accessToken']
    r = requests.get(url='https://www.googleapis.com/plus/v1/people/'+access_token+"?key=AIzaSyBE6g7iOti0MNkKUqCSsKUy1BJWtvCuBfY")
    data = r.json()
    if (data.get("id")):
        print(access_token)
        user = User.objects.filter(username=data.get('id')).first()
        if not user:
            user = User.objects.create_user(email=request.data["email"],username=data.get('id'),password=uuid.uuid4(),last_name=data.get('displayName'))
            image_url = data.get("image").get("url")
            print("cscasd",data,data.get("image"))
            AppUser.objects.create(user=user,is_invidual=request.data["is_invidual"],image_url=image_url)
        appUser = AppUser.objects.filter(user_id=user.id).first()
        if appUser:
            user.position = appUser.position
            user.is_invidual = appUser.is_invidual
            user.birthday = appUser.birthday
            user.image_url = appUser.image_url
        serializer = AppUserSerializer(user)
        password = serializer.data.pop("password")
        return Response(serializer.data)
    else:
        data = {
            "message": "Can not login Google"
        }
        return Response(data=data, status=status.HTTP_400_BAD_REQUEST)
@api_view(['POST'])
def interest_SDGs(request):
    try:
        print(request.data)
        user_id = request.data.get("userId")
        user = AppUser.objects.filter(user_id=user_id).first()
        SDGS = request.data["SDGS"]
        interested_SDGs = []
        for SDG in SDGS:
            main_target = MainTarget.objects.filter(id=SDG).first()
            interested_SDGs.append(main_target)
        user.interested_SDGs.set(interested_SDGs)
        user.save()
        data = {
            "message": "Update successfully"
        }
        return Response(data=data,status=status.HTTP_200_OK)
    except Exception as e:
        print(e)
        return Response(status=status.HTTP_400_BAD_REQUEST)
@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def login(request):
    mail = request.data['email']
    password = request.data['password']
    get_user = User.objects.filter(email=mail).first()
    print(get_user)
    if get_user is None:
        data = {
            "message": "Email doesn't exist"
        }
        return Response(data=data,status=status.HTTP_400_BAD_REQUEST)
    username = get_user.username
    user = authenticate(username=username, password=password)
    print(user)
    if user is not None:
        # the password verified for the user
        if user.is_active:
            user.last_name = user.last_name
            serializer = AppUserSerializer(user)
            data = serializer.data
            password = data.pop("password")
            return Response(data)
    data = {
        "message": "Email or password is wrong"
    }
    return Response(data=data,status=status.HTTP_400_BAD_REQUEST)
@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def changePassword(request):
    id = request.data["userId"]
    password = request.data["password"]
    old_password = request.data["old_password"]
    user = User.objects.filter(id=id).first()
    success = user.check_password(old_password)
    if success:
        user.set_password(password)
        user.save()
        data = {
            "message": "Update successfully"
        }
        return Response(data=data, status=status.HTTP_200_OK)
    else:
        data = {
            "message": "Wrong old password"
        }
        return Response(data=data, status=status.HTTP_400_BAD_REQUEST)
@api_view(['GET'])
@authentication_classes([])
@permission_classes([])
def searchUser(request):
    userMail = request.GET.get('userMail')
    userName = request.GET.get('userName')
    orgType = request.GET.get('orgType')
    isInvidual = request.GET.get("isInvidual")
    users = AppUser.objects.all()
    if userMail is not None or userName is not None:
        if userMail is not None and userName is not None:
            users = users.filter(Q(user__email__contains=userMail) | Q(user__username__contains=userName))
        else:
            if userName is not None:
                users = users.filter(user__username__contains=userName)
            if userMail is not None:
                users = users.filter(user__email__contains=userMail)

    if orgType is not None and orgType != '':
        users = users.filter(org_type=orgType)
    if isInvidual:
        users = users.filter(is_invidual=True)
    serializer = UserInfoSerialzer(users,many=True)
    data = serializer.data
    return Response(data=data, status=status.HTTP_200_OK)

@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def updateUser(request):
    id = request.data["userId"]
    mail = request.data['email']
    last_name = request.data["last_name"]
    username = request.data["username"]
    location = request.data["location"]
    phone = request.data["phone"]
    birthday = request.data["birthday"]
    org_type = request.data["org_type"]
    website=request.data["website"]
    intro = request.data["intro"]
    image_url = request.data["image_url"]
    appuser = AppUser.objects.filter(user=id).first()
    if mail != "":
        user_check = User.objects.filter(email=mail).first()
        if user_check is None:
            user = User.objects.filter(id=id).first()
            user.email = mail
            user.save()
        else:
            data = {
                "message": "Email already taken"
            }
            email_check = user_check.email
            if email_check == mail:
                data = {
                    "message": "Email is your"
                }
            return Response(data=data, status=status.HTTP_400_BAD_REQUEST)
    if last_name != "":
        appuser.user.last_name = last_name
    if username != "":
        user_check = User.objects.filter(username=username).first()
        if user_check is None:
            user = User.objects.filter(id=id).first()
            user.username = username
            user.save()
        else:
            data = {
                "message": "Username already taken"
            }
            return Response(data=data,status=status.HTTP_400_BAD_REQUEST)
    if location != "":
        appuser.location = location
    if phone is not None:
        appuser.phone = phone
    if birthday != "":
        appuser.birthday = birthday
    if intro is not None:
        appuser.intro = intro
    if website is not None:
        appuser.website = website
    if image_url != "":
        appuser.image_url = image_url
    if org_type != "":
        appuser.org_type = org_type
    appuser.save()
    data = {
        "message": "Update successfully"
    }
    return Response(data=data,status=status.HTTP_200_OK)
class IdeaAPI(APIView):
    @api_view(['POST'])
    def creat_idea(request):
        user = AppUser.objects.filter(user_id=request.data["userId"]).first()
        images = request.data.pop("images")
        images = json.dumps(images)
        Idea.objects.create(user=user,content=request.data["content"],images=images)
        data = {
            "message": "Create idea successfully"
        }
        return Response(data,status=status.HTTP_201_CREATED)
    @api_view(['POST'])
    def edit_idea(request):
        images = request.data.pop("images")
        images = json.dumps(images)
        ideaId = request.data["ideaId"]
        content = request.data["content"]
        idea = Idea.objects.filter(id=ideaId).first()
        idea.images = images
        idea.content = content
        idea.save()
        data = {
            "message": "Edit idea successfully"
        }
        return Response(data, status=status.HTTP_200_OK)

    @api_view(['POST'])
    def delete_idea(request):
        ideaIds = request.data["ideas"]
        for ideaId in ideaIds:
            idea = Idea.objects.filter(id=ideaId).first()
            idea.onDelete = True
            idea.save()
        data = {
            "message": "Delete idea successfully"
        }
        return Response(data, status=status.HTTP_200_OK)
    @api_view(['POST'])
    def like_idea(request):
        ideaId = request.data["ideaId"]
        userId = request.data["userId"]
        user = AppUser.objects.filter(user__id=userId).first()
        idea = Idea.objects.filter(id=ideaId).first()
        data = ""
        if user in idea.likers.all():
            idea.likers.remove(user)
            data = {
                "message": "Unlike idea successfully"
            }
        else:
            idea.likers.add(user)
            data = {
                "message": "Like idea successfully"
            }
        idea.save()

        return Response(data, status=status.HTTP_200_OK)
    @api_view(['GET'])
    def get_ideas(request):
        userId = request.GET.get('userId','')
        print("user",userId)
        ideas = Idea.objects.filter(user_id=userId).filter(onDelete=False)
        serializer = IdeaSerializer(ideas,many=True)
        return Response(serializer.data)
class MainTargetList(APIView):
    def get(self,request):
        mains_target = MainTarget.objects.all()
        return Response(MainTargetListSerializer(mains_target,many=True).data)
@permission_classes((permissions.AllowAny,))
class AritcaleDetail(generics.RetrieveAPIView):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
class ActionsAPI(APIView):
    @api_view(['POST'])
    def creat_action(request):
        print(request)
        user = AppUser.objects.filter(user_id=request.data["userId"]).first()
        userID = request.data.pop("userId")
        SDGIds = request.data.pop("SDGId")
        actions = request.data.pop("actions")
        actions = json.dumps(actions)
        images = request.data.pop("images")
        images = json.dumps(images)
        search_name = request.data["name"]
        search_name = remove_accents(search_name)
        data = request.data
        print("asdsadasda",SDGIds)
        action = Action.objects.create(user=user,images=images,search_name=search_name,actions=actions,**data)
        for SDGId in SDGIds:
            SDG = MainTarget.objects.filter(id=SDGId).first()
            action.SDG.add(SDG)
        action.save()
        # users = AppUser.objects.all()
        # for user in users:
        #     action.folow_users.add(user)
        # action.save()
        data = {
            "message": "Create actions successfully"
        }
        return Response(data)

    @api_view(['POST'])
    def edit_action(request):
        print(request)
        actionId = request.data.pop("actionId")
        SDGIds = request.data.pop("SDGId")
        actions = request.data.pop("actions")
        actions = json.dumps(actions)
        images = request.data.pop("images")
        images = json.dumps(images)
        search_name = request.data["name"]
        search_name = remove_accents(search_name)
        data = request.data
        Action.objects.filter(id=actionId).update(images=images, search_name=search_name, actions=actions, **data)
        action = Action.objects.filter(id=actionId).first()
        action.SDG.clear()
        for SDGId in SDGIds:
            SDG = MainTarget.objects.filter(id=SDGId).first()
            action.SDG.add(SDG)
        action.save()
        data = {
            "message": "Edit action successfully"
        }
        return Response(data)
    @api_view(['POST'])
    def private_action(request):
        print(request)
        actionIds = request.data["actions"]
        for actionId in actionIds:
            action = Action.objects.filter(id=actionId).first()
            action.isPrivate = True
            action.save()
        data = {
            "message": "Private actions successfully"
        }
        return Response(data)
    @api_view(['POST'])
    def delete_action(request):
        print(request)
        actionIds = request.data["actions"]
        for actionId in actionIds:
            action = Action.objects.filter(id=actionId).first()
            action.onDelete = True
            action.save()
        data = {
            "message": "Delete actions successfully"
        }
        return Response(data)
    @api_view(['GET'])
    def get_actions(request):
        userId = request.GET.get('userId','')
        SDG = request.GET.get('SDG',"")
        actions = Action.objects.filter(onDelete=False)
        if userId != '':
            actions = actions.filter(user_id = userId)
        if SDG != '':
            actions = actions.filter(SDG_id = SDG)
        serializer = ActionSerializer(actions,many=True)
        return Response(serializer.data,status=status.HTTP_200_OK)
    @api_view(['GET'])
    @authentication_classes([])
    @permission_classes([])
    def auto_fill(request):
        userMail = request.GET.get('userMail')
        userName = request.GET.get("userName")
        SDGId = request.GET.get("SDGId")
        keyword = request.GET.get("keyword")
        keyword = remove_accents(keyword)
        actions = Action.objects.filter(onDelete=False)
        if userMail is not None and userMail != '':
            actions = actions.filter(user__user__email__contains=userMail)
        if userName is not None and userName != '':
            actions = actions.filter(user__user__email__contains=userName)
        if SDGId is not None and SDGId != '':
            print("SDG", keyword)
            actions = actions.filter(SDG__in=[SDGId])
        if keyword is not None and keyword!='':
            print("keyword", keyword)
            actions = actions.filter(search_name__contains=keyword)
        serializer = ActionSerializer(actions, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @api_view(['GET'])
    @authentication_classes([])
    @permission_classes([])
    def action_detail(request):
        actionId = request.GET.get('actionId')
        print(21321321321394540524)
        action = Action.objects.filter(id=actionId).first()
        serializer = ActionSerializer(action)
        return Response(serializer.data, status=status.HTTP_200_OK)
    @api_view(["POST"])
    def interestAction(request):
        try:
            print(request.data)
            action_id= request.data.get("actionId")
            user_id = request.data.get("userId")
            user = AppUser.objects.filter(user_id=user_id).first()
            action = Action.objects.filter(id=action_id).first()
            data = ""
            if user in action.follow_users.all():
                action.follow_users.remove(user)
                data = {
                    "message": "Interest action successfully"
                }
            else:
                action.follow_users.add(user)
                data = {
                    "message": "None interest action successfully"
                }
            action.save()
            return Response(status=status.HTTP_200_OK,data=data)
        except Exception as e:
            print(e)
            return Response(status=status.HTTP_400_BAD_REQUEST)

