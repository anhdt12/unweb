# Generated by Django 2.0 on 2017-12-25 09:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0008_idea'),
    ]

    operations = [
        migrations.CreateModel(
            name='Action',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_time', models.DateTimeField(auto_now_add=True)),
                ('name', models.CharField(max_length=200)),
                ('target', models.CharField(max_length=200)),
                ('location_lat', models.FloatField()),
                ('location_lng', models.FloatField()),
                ('start_time', models.DateField()),
                ('end_time', models.DateField()),
                ('actions', models.TextField()),
                ('status', models.CharField(choices=[('Incoming', 'ICM'), ('Doing', 'DI'), ('Done', 'DN')], max_length=200)),
                ('description', models.TextField()),
                ('images', models.TextField()),
            ],
        ),
        migrations.AddField(
            model_name='maintarget',
            name='inactive_image_url',
            field=models.CharField(blank=True, max_length=800, null=True),
        ),
        migrations.AlterField(
            model_name='maintarget',
            name='img_url',
            field=models.CharField(blank=True, max_length=800, null=True),
        ),
        migrations.AddField(
            model_name='action',
            name='SDG',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.MainTarget'),
        ),
        migrations.AddField(
            model_name='action',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.AppUser'),
        ),
    ]
