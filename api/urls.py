from django.conf import settings
from django.conf.urls import url, static
from rest_framework.authtoken import views as rest_framework_views

from api import views as local_view


urlpatterns = [
    # Session Login
    #url(r'^test/$', local_view.test_celery, name='test'),
    url(r'^get_auth_token/$', rest_framework_views.obtain_auth_token, name='get_auth_token'),
    url(r'^getAllSlider/',local_view.SliderList.as_view()),
    url(r'^getPost/$',local_view.PostList.as_view()),
    url(r'^getAllTargets/', local_view.TargetList.as_view()),
    url(r'^getArticale/$', local_view.ArticleList.as_view()),
    url(r'^getArticaleDetail/(?P<pk>[0-9]+)/$', local_view.AritcaleDetail.as_view()),
    url(r'^addArticale', local_view.ArticlePost.as_view()),
    url(r'^editArticle', local_view.edit_article),
    url(r'^connectGame', local_view.GameConnect.as_view()),
    url(r'^signUp', local_view.create_user),
    url(r'^login', local_view.login,name='login'),
    url(r'^updateUser', local_view.updateUser),
    url(r'^changePassword', local_view.changePassword),
    url(r'^loggingFacebook', local_view.loggingFacebook),
    url(r'^loggingGoogle', local_view.loggingGoogle),
    url(r'^postIdea', local_view.IdeaAPI.creat_idea),
    url(r'^editIdea', local_view.IdeaAPI.edit_idea),
    url(r'^likeIdea', local_view.IdeaAPI.like_idea),
    url(r'^deleteIdea', local_view.IdeaAPI.delete_idea),
    url(r'^getIdeas/$', local_view.IdeaAPI.get_ideas),
    url(r'^getListTarget', local_view.MainTargetList.as_view()),
    url(r'^postAction', local_view.ActionsAPI.creat_action),
    url(r'^editAction', local_view.ActionsAPI.edit_action),
    url(r'^getActions/$', local_view.ActionsAPI.get_actions),
    url(r'^actionDetail/$', local_view.ActionsAPI.action_detail),
    url(r'^searchAction/$', local_view.ActionsAPI.auto_fill),
    url(r'^privateAction', local_view.ActionsAPI.private_action),
    url(r'^deleteAction', local_view.ActionsAPI.delete_action),
    url(r'^searchUser/$', local_view.searchUser),
    url(r'^interestAction', local_view.ActionsAPI.interestAction),
    url(r'^interestSDGs', local_view.interest_SDGs),
    url(r'^getBlogs', local_view.get_Blog),
    url(r'^getUserbyname/$', local_view.getUserbyName),
    url(r'^searchVideos/$', local_view.search_video),
    url(r'^twitterLogin/$', local_view.twitterLogin),
    url(r'^twitterRequest', local_view.twitterRequest),

]