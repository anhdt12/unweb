# Generated by Django 2.0 on 2017-12-22 06:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0005_appuser_birth_day'),
    ]

    operations = [
        migrations.RenameField(
            model_name='appuser',
            old_name='birth_day',
            new_name='birthday',
        ),
    ]
