from django.contrib import admin
from django_summernote.widgets import SummernoteWidget
from django import forms

from api.models import Post,Slider,ImageSlider,Target,MainTarget,Collection,Article,ConnectGame,Action
class ImageSliderInline(admin.TabularInline):
    model = ImageSlider

class SilderAdmin(admin.ModelAdmin):
    inlines = [
        ImageSliderInline,
    ]
    search_fields = ("slider_name",)

class PostAdminForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = '__all__'
class TargetInline(admin.TabularInline):
    model = Target
    exclude = ('main_target',)
class TargetAdmin(admin.ModelAdmin):
    inlines = [
        TargetInline,
    ]
    search_fields = ("titile",)

class PostAdmin(admin.ModelAdmin):
    form = PostAdminForm
    search_fields = ("post_name","header_name")

class MainTargetAdmin(admin.ModelAdmin):
    search_fields = ("title",)
class CollectionAdmin(admin.ModelAdmin):
    search_fields = ("collection_name",)
class ArticleAdmin(admin.ModelAdmin):
    search_fields = ("article_name",)
class ActionAdmin(admin.ModelAdmin):
    search_fields = ("name",)

# Register your models here.
admin.site.register(Post,PostAdmin)
admin.site.register(Slider,SilderAdmin)
admin.site.register(ImageSlider)
admin.site.register(Target,TargetAdmin)
admin.site.register(MainTarget,MainTargetAdmin)
admin.site.register(Collection,CollectionAdmin)
admin.site.register(Article,ArticleAdmin)
admin.site.register(ConnectGame)
admin.site.register(Action,ActionAdmin)
