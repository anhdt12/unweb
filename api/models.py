from django.contrib.auth.models import User
from django.db import models
from tinymce.models import HTMLField
from rest_framework.authtoken.models import Token
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
import json

STATUS_CHOICE = (('Incoming', 'ICM'),('Doing','DI'),('Done','DN'))

# Create your models here.
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
# Create your models here.
class Post(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    post_name = models.CharField(max_length=200)
    header_name = models.CharField(max_length=500)
    post_content = HTMLField()
    page = models.CharField(max_length=200)
    class Meta:
        ordering = ('created_time',)
        verbose_name = "Homepage"
        verbose_name_plural = 'Homepage'


    def __str__(self):
        return self.post_name
class AppUser(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE,primary_key=True)
    created_time = models.DateTimeField(auto_now_add=True)
    position = models.CharField(max_length=200)
    birthday = models.DateField(null=True,blank=True)
    is_invidual = models.BooleanField()
    image_url = models.CharField(max_length=300)
    phone = models.CharField(max_length=50,default="")
    location = models.CharField(max_length=200,default="")
    website = models.CharField(max_length=200,default="")
    intro = models.TextField(default="")
    org_type = models.CharField(default="",max_length=200)
    interested_SDGs = models.ManyToManyField("MainTarget",related_name='interested_SDGs',blank=True,null=True)
    class Meta:
        ordering = ('created_time',)
    def __str__(self):
        return self.user.last_name

class Idea(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    content = models.TextField()
    likers = models.ManyToManyField(AppUser,related_name='likers')
    images = models.TextField(default="test")
    onDelete = models.BooleanField(default=False)
    user = models.ForeignKey(AppUser,on_delete=models.CASCADE)
    class Meta:
        ordering = ('-created_time',)


class ConnectGame(models.Model):
    created_time = models.DateTimeField(auto_now_add=True,)
    img_url = models.CharField(max_length=300)
    success_img_url = models.CharField(max_length=300,default="")
    label = models.TextField(blank=True, null=True)
    class Meta:
        ordering = ('created_time',)
        verbose_name_plural = "Game"

    def game_data(self):
        return "some_calculated_result"
    def __str__(self):
        return self.label
    
class Collection(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    display_name = models.CharField(max_length=200,default="")
    collection_name = models.CharField(max_length=200)
    collection_type = models.CharField(max_length=200,default="SYSTEM")
    short_descripton = models.CharField(max_length=200,blank=True,null=True)
    long_descripton = models.CharField(max_length=500,blank=True,null=True)
    img_url = models.CharField(max_length=300,blank=True,null=True)
    class Meta:
        ordering = ('created_time',)
        verbose_name = "Post folder"
    def __str__(self):
        if self.display_name == "":
            return self.collection_name
        else:
            return self.display_name


class MainTarget(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=200, null=True)
    slogan = models.CharField(max_length=200, blank=True, null=True)
    img_url = models.CharField(max_length=800, blank=True, null=True)
    inactive_image_url = models.CharField(max_length=800,blank=True,null=True)
    banner_url = models.CharField(max_length=300, blank=True, null=True)
    class Meta:
        ordering = ('created_time',)
        verbose_name = 'The "Goal" Tiltle'
        verbose_name_plural = 'The "Goal" Tiltle'

    def childTarget(self):
        return "some_calculated_result"

    def __str__(self):
        return self.title


class Target(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=200,null=True)
    main_target = models.ForeignKey(MainTarget,on_delete=models.CASCADE,blank=True,null=True,)
    parentTarget = models.ForeignKey("Target",on_delete=models.CASCADE,blank=True,null=True,)
    description = models.TextField(blank=True, null=True)
    
    class Meta:
        ordering = ('created_time',)
        verbose_name = 'The "Goal" Content'
        verbose_name_plural = 'The "Goal" Content'
    def childTarget(self):
        return "some_calculated_result"
    def __str__(self):
        if (self.parentTarget):
            return self.parentTarget.title + "." + self.title
        return self.title
    
class Article(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    collection = models.ForeignKey(Collection, on_delete=models.CASCADE)
    article_name = models.CharField(max_length=500)
    search_article_name = models.CharField(max_length=500,default="")
    user = models.ForeignKey(User,blank=True,null=True, on_delete=models.CASCADE)
    img_url = models.CharField(max_length=300,blank=True,null=True)
    download_url = models.CharField(max_length=500,default="",blank=True,null=True)
    article_content = HTMLField()
    descrition = models.TextField(default="")
    def meta_data(self):
        return "some_calculated_result"
    class Meta:
        ordering = ('-created_time',)
        verbose_name = "Post"
    def __str__(self):
        return self.article_name
class ArticleMeta(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    key = models.CharField(max_length=50)
    key_title = models.CharField(max_length=200)
    content = models.CharField(max_length=50)
    content_title = models.CharField(max_length=200)
    def __str__(self):
        return self.key
class Slider(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    page = models.CharField(max_length=200)
    slider_name = models.CharField(max_length=200)
    position = models.IntegerField()
    class Meta:
        ordering = ('created_time',)
        verbose_name_plural = "Homepage Slider"
        verbose_name = "Homepage Slider"
    def images(self):
        return "some_calculated_result"
    def __str__(self):
        return self.slider_name
class ImageSlider(models.Model):
    created_time = models.DateTimeField(auto_now_add=True,)
    slider = models.ForeignKey(Slider, on_delete=models.CASCADE)
    img_url = models.CharField(max_length=200)
    action = models.ForeignKey("Action",on_delete=models.CASCADE,null=True,blank=True)
    blog = models.ForeignKey("Article",on_delete=models.CASCADE,null=True,blank=True)
    position = models.IntegerField()
    label = models.CharField(max_length=500,blank=True,null=True)
    class Meta:
        ordering = ('created_time',)
        verbose_name = "Homepage Image Slide"

    def __str__(self):
        if self.label is not None:
            return self.label
        else :
            return "Slider"

class Action(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(AppUser,on_delete=models.CASCADE)
    name = models.CharField(max_length=500)
    search_name = models.CharField(max_length=500,default="")
    SDG = models.ManyToManyField(MainTarget,related_name='SDGs')
    target = models.CharField(max_length=200)
    location_lat = models.FloatField(blank=True,null=True)
    location_lng = models.FloatField(blank=True,null=True)
    start_time = models.DateField(blank=True,null=True)
    end_time = models.DateField(blank=True,null=True)
    actions = models.TextField()
    location_name = models.TextField()
    isPrivate = models.BooleanField(default=False)
    onDelete = models.BooleanField(default=False)
    status = models.CharField(max_length=200)
    description = models.TextField()
    images = models.TextField()
    follow_users = models.ManyToManyField(AppUser,related_name='followers')
    class Meta:
        ordering = ('-created_time',)
        verbose_name = "SDG Action"
    def getactions(self):
        return json.load(self.actions)
    def getimages(self):
        return json.loads(self.images)
    def __str__(self):
        return self.name
